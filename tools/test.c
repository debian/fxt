/*
 *  Copyright (C) 2016-2017 Universite de Bordeaux
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define CONFIG_FUT
#if defined(_WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#else
#include <pthread.h>
#endif
#include <fut.h>
#include <stdlib.h>

#define BUFFER_SIZE (1<<20)

long myid(void)
{
#if defined(_WIN32) && !defined(__CYGWIN__)
	return (long) GetCurrentThreadId();
#else
	return (long) pthread_self();
#endif
}

int main(void) {
	fprintf(stderr,"setting filename\n");
	fut_set_filename("temp");
	fprintf(stderr,"setting flush\n");
	enable_fut_flush();
	fprintf(stderr,"setting up\n");
	if (fut_setup(BUFFER_SIZE, 0xffff, myid()) < 0)
	{
		perror("fut_setup");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"setting key\n");
	if (fut_keychange(FUT_ENABLE, 0x7fffff, myid()) < 0)
	{
		perror("fut_keychange");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"setting filename again\n");
	fut_set_filename("temp2");
	fprintf(stderr,"running probes\n");
	FUT_PROBE1(0x1, 0x101, 1234);
	FUT_PROBE2(0x1, 0x102, 1234, 1234);
	FUT_PROBE3(0x1, 0x103, 1234, 1234, 1234);
	FUT_PROBE4(0x1, 0x104, 1234, 1234, 1234, 1234);
	FUT_PROBE5(0x1, 0x105, 1234, 1234, 1234, 1234, 1234);
	FUT_PROBE6(0x1, 0x106, 1234, 1234, 1234, 1234, 1234, 1234);
	FUT_PROBE7(0x1, 0x107, 1234, 1234, 1234, 1234, 1234, 1234, 1234);

	fprintf(stderr,"running string probe\n");
	FUT_PROBESTR(0x1, 0x200, "foof");
	fprintf(stderr,"ending up\n");
	if (fut_endup("temp2") < 0)
	{
		perror("fut_endup");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"done\n");
	if (fut_done() < 0)
	{
		perror("fut_done");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"ok\n");
	if (system("./fxt_print -f temp2 > fxt_print.log") != 0) {
		fprintf(stderr,"error fxt_print");
		exit(EXIT_FAILURE);
	}
	if (system("grep \"4294967295	             107(7)	         unknown code 107	4d2	4d2	4d2	4d2	4d2	4d2	4d2\" fxt_print.log") != 0) {
		system("cat fxt_print.log");
		fprintf(stderr,"error fxt_print string output");
		exit(EXIT_FAILURE);
	}
	if (system("grep \"4294967295	             200(1)	         unknown code 200	666f6f66\" fxt_print.log") != 0) {
		system("cat fxt_print.log");
		fprintf(stderr,"error fxt_print string output");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
